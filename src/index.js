// my first comment
console.log('hello world!');


// declare variable
let myName = 'Alex Harral';
console.log(myName);

// declare constant variable
const rate = 0.3;
//rate = 1;
console.log(rate)

// this finds an HTML element with the id="demo" and changes the element 
// content to "Hello Javascript"
document.getElementById("demo").innerHTML = "Hello Javascript";
